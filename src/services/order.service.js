const { AppError } = require("../helpers/error");
const { Order, User, Food } = require("../models");

const createOrder = async (userId, foodId, data) => {
  try {
    const user = await User.findOne({
      where: {
        id: userId,
      },
    });
    if (!user) {
      throw new AppError(400, "user not found");
    }
    const food = await Food.findOne({
      where: {
        food_id: foodId,
      },
    });
    if (!food) {
      throw new AppError(400, "food not found");
    }

    const createdOrder = await User.create(data);
    return createdOrder;
  } catch (error) {
    console.error(error);
    throw error;
  }
};
module.exports = {
  createOrder,
};
