const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define(
    "Order",
    {
      foodId: {
        type: DataTypes.INTEGER,

        field: "food_id",
      },
      foodName: {
        type: DataTypes.STRING,
        allowNull: false,
        field: "food_id",
      },
      image: {
        type: DataTypes.STRING,
      },
      price: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      desc: {
        type: DataTypes.STRING,
      },
    },
    {
      tableName: "orders",
      // disable createdAt, updatedAt
      timestamps: false,
    }
  );
};
