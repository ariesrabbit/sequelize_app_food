// Setup Sequelize
const { Sequelize } = require("sequelize");

const sequelize = new Sequelize("app_food_sequelize", "root", "1234", {
  dialect: "mysql",
  host: "localhost",
  port: 3306,
});

(async () => {
  try {
    await sequelize.authenticate();
    console.log("Sequelize Connected");
  } catch (error) {
    console.log("Sequelize Error", error);
  }
})();

// Khởi tạo model
const User = require("./User")(sequelize);
const Restaurant = require("./Restaurant")(sequelize);
const RestaurantLikes = require("./RestaurantLikes")(sequelize);
const Order = require("./Order")(sequelize);
const Food = require("./Food")(sequelize);

// Định nghĩa relationship giữa các model

// User 1 - n Restaurant
Restaurant.belongsTo(User, { as: "owner", foreignKey: "userId" });
User.hasMany(Restaurant, { as: "restaurants", foreignKey: "userId" });
// User - Order
Order.belongsTo(User, { as: "Order", foreignKey: "userId" });
User.hasMany(Order, { as: "Order", foreignKey: "userId" });
// Food - Oder
Order.belongsTo(Food, { as: "Food", foreignKey: "foodId" });
Food.hasMany(Order, { as: "Food", foreignKey: "foodId" });

// User 1 - n RestaurantLikes
// Restaurant 1 - n RestaurantLikes
User.belongsToMany(Restaurant, {
  as: "restaurantLikes",
  through: RestaurantLikes,
  foreignKey: "userId",
});
Restaurant.belongsToMany(User, {
  as: "userLikes",
  through: RestaurantLikes,
  foreignKey: "restaurantId",
});

module.exports = {
  sequelize,
  User,
  Restaurant,
  Order,
  Food,
};
