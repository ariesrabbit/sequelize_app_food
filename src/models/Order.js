const { DataTypes, Sequelize } = require("sequelize");

module.exports = (sequelize) => {
  return sequelize.define(
    "Order",
    {
      userId: {
        type: DataTypes.INTEGER,
        field: "user_id",
      },
      foodId: {
        type: DataTypes.INTEGER,
        field: "food_id",
      },
      amount: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      code: {
        type: DataTypes.STRING,
      },
      createdAt: {
        type: DataTypes.DATE,
        field: "created_at",
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      },
    },
    {
      tableName: "orders",
      // disable createdAt, updatedAt
      timestamps: false,
    }
  );
};
