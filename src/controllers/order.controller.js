const { response } = require("../helpers/response");
const orderService = require("../services/order.service");

// POST localhost:4000/restaurants/:restaurantId/like - body: {userId: 1}
const createOrder = () => {
  return async (req, res, next) => {
    try {
      const dataOrder = req.params;
      const { userId } = req.params;
      const { foodId } = req.body;
      const createdOrder = await orderService.createOrder(
        userId,
        foodId,
        dataOrder
      );
      res.status(200).json(response(createdOrder));
    } catch (error) {
      next(error);
    }
  };
};

module.exports = {
  createOrder,
};
