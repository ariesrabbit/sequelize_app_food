// Routers V1
const express = require("express");

const authController = require("../../controllers/auth.controller");

// path v1: /api/v1
const v1 = express.Router();
const userRouters = require("./user.router");
const restaurantRouter = require("./restaurant.router");
const orderRouter = require("./order");
// user
v1.use("/users", userRouters);

// restaurant
v1.use("/restaurants", restaurantRouter);
// Định nghĩa các routers cho foods
// order
v1.use("/orders", orderRouter);
// Định nghĩa các routers cho auth
v1.post("/login", authController.login());

module.exports = v1;
