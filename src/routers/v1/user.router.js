const express = require("express");

const userController = require("../../controllers/user.controller");
const userRouters = express.Router();

userRouters.get("", userController.getUsers());
userRouters.get("/:id", userController.getUserByID());
userRouters.post("", userController.createUser());
userRouters.put("/:id", userController.updateUser());
userRouters.delete("/:id", userController.deleteUser());

module.exports = userRouters;
