const express = require("express");

const restaurantController = require("../../controllers/restaurant.controller");
const restaurantRouter = express.Router();

restaurantRouter.get("", restaurantController.getRestaurants());
restaurantRouter.post(
  "/:restaurantId/like",
  restaurantController.likeRestaurant()
);

module.exports = restaurantRouter;
